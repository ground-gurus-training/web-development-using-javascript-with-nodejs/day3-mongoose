const mongoose = require('../db');

const noteSchema = mongoose.Schema({
    note: String,
    dateCreated: Date
});

const Note = mongoose.model('Note', noteSchema);

module.exports = Note;
