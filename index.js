const exphbs = require('express-handlebars');

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const mongoose = require('./db');

const Note = require('./schemas/note');

app.engine('handlebars', exphbs({
    runtimeOptions: {
        allowProtoPropertiesByDefault: true,
        allowProtoMethodsByDefault: true
    }
}));
app.set('view engine', 'handlebars');

app.get('/', async (req, res) => {
    const result = await Note.find({}, (err, data) => {
        return JSON.stringify(data);
    });

    res.render('home', {
        showTitle: true,
        helpers: {
            notes: () => {
                return result;
            }
        }
    });
});

app.post('/api/note', (req, res) => {
    const newNote = new Note();

    newNote.note = req.body.note;
    newNote.dateCreated = new Date();

    newNote.save(() => {
        res.redirect('/');
    });
});

app.listen(port, () => {
    console.log(`Mongoose example running at http://localhost:${port}`);
});
